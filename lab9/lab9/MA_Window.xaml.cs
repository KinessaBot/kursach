﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace lab9
{
    /// <summary>
    /// Interaction logic for MA_Window.xaml
    /// </summary>
    public partial class MA_Window : Window, Owning
    {
        public MA_Window()
        {
            InitializeComponent();
        }
        public void SetVal(int kod,int ctn = 1)
        {
            KoV.Text =""+ kod;

        }

        public void SetVal(DataRowView obj, int ctn = 1)
        {
            KoV.Text = obj[0].ToString();
            Ops.Text = obj[1].ToString();
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            if (SC.QueryExecuter("insert into Мероприятие(Название,КодВида,ДатаПроведения) values(@Naz,@KoV,@DpM)", SB, new Parametr("@Naz", Naz.Text),
            new Parametr("@KoV", KoV.Text), new Parametr("@DpM", DpM.DisplayDate)))
            {
                int kod = SC.IDS("Select Max(Код) From Мероприятие Where Название=@Naz and КодВида=@KoV and ДатаПроведения=@DpM"
                       , new Parametr("@Naz", Naz.Text), new Parametr("@KoV", KoV.Text), new Parametr("@DpM", DpM.DisplayDate));
                using (FileStream fs = new FileStream("Log.json", FileMode.Append))
                {
                        SC.jsonFor.WriteObject(fs, new LogJSON("Мероприятие", LogJSON.Action.Update,
                            DateTime.Now, new Column("Код", null, kod, true),
                            new Column("КодВида", null, int.Parse(KoV.Text), true),
                            new Column("ДатаПроведения", null, DpM.DisplayDate, true),
                            new Column("Название", null, Naz.Text, true)));
                }
                if (Owner != null)
                {

                    ((Owning)Owner).SetVal(kod, 6);
                    Close();
                }
                Naz.Text = "";
                KoV.Text = "";
                Ops.Text = "";
                DpM.Text = "";
            }
            
        }

        private void KoV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                VM_WINDOW w = new VM_WINDOW();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoV.Text = "" + SC.IDS("Select top 1 Код From ВидМероприятия Where Описание like @Ops Order by 1",
                    new Parametr("@ops", "%" + Ops.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                VMA_Window w = new VMA_Window();
                w.Owner = this;
                w.Show();
            }
        }
    }
}
