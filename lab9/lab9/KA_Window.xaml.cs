﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for KA_Window.xaml
    /// </summary>
    public partial class KA_Window : Window
    {
        public KA_Window()
        {
            InitializeComponent();
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter("Insert into Комната(Номер,Состояние) values(@Nom,@Sos)", SB, new SqlParameter("@Nom", Nom.Text)
                , new SqlParameter("@Sos",Sos.IsChecked));
            if (Owner != null)
            {
                ((Owning)Owner).SetVal(SC.IDS("Select Max(Код) From Комната Where Номер=@Nom", new Parametr("@Nom", Nom.Text)),4);
                Close();
            }
            Nom.Text = "";
            Sos.IsChecked = false;
        }

        private void Nom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.D0 | e.Key == Key.D1 | e.Key == Key.D2 | e.Key == Key.D3 | e.Key == Key.D4 |
                e.Key == Key.D5 | e.Key == Key.D6 | e.Key == Key.D7 | e.Key == Key.D8 | e.Key == Key.D9 |
                e.Key == Key.OemMinus | e.Key == Key.Back)
                return;
            else
                e.Handled = true;
        }
    }
}
