﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for O_Window.xaml
    /// </summary>
    public partial class O_Window : Window
    {
        DataTable O;
        SqlDataAdapter adap;

        public O_Window()
        {
            InitializeComponent();
        }

        private void ShowDG()
        {
            SC.QueryExecuter("exec OS @Kod, @Naz", DG, out O, ref adap, TB,new Parametr("@Kod", "%" + Kod.Text + "%"),
               new Parametr("@Naz", "%" + Naz.Text + "%"));
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            SC.UpdateDB(adap, O, TB);
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            SC.DeleteRows(DG, adap, O, TB);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if(DG.SelectedItem != null)
                ((Owning)Owner).SetVal((DataRowView)DG.SelectedItem);
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }
    }
}
