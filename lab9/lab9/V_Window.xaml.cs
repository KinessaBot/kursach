﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for V_Window.xaml
    /// </summary>
    public partial class V_Window : Window, Owning
    {
        DataTable O;
        SqlDataAdapter adap;

        public void SetVal(DataRowView obj, int ctn = 1)
        {
            KoO.Text = obj[0].ToString();
            Naz.Text = obj[1].ToString();
        }

        public void SetVal(int kod, int ctn = 1)
        {
            KoO.Text = "" + kod;
        }

        public V_Window()
        {
            InitializeComponent();
            DG.IsReadOnly = true;
        }

        private void ShowDG()
        {
            if (OkM.Text == "") OkM.Text = "999999999";
            if (OkB.Text == "") OkB.Text = "-999999999";
            SC.QueryExecuter("exec VS @Kod, @FIO, @KoO, @OkM, @OkB, @Naz", DG, out O, ref adap, TB, new Parametr("@Kod", "%" + Kod.Text + "%"),
               new Parametr("@Naz", "%" + Naz.Text + "%"), new Parametr("@FIO", "%" + FIO.Text + "%"), new Parametr("@KoO", "%" + KoO.Text + "%"),
               new Parametr("@OkM", OkM.Text), new Parametr("@OkB", OkB.Text));
            KoO.Text = "";
            Naz.Text = "";
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            bool par1 = false, par2 = false, par3 = false;
            if (KoO.Text != "") par1 = true;
            if (FIO.Text != "") par2 = true;
            if (OkM.Text != "999999999") par3 = true;
            if (!par1 && !par2 && !par3)
                TB.Text = "Заполните хотя бы одно поле для изменения";
            SC.UpdateDB(DG, TB, "Вожатый", new Parametr("КодОтряда", KoO.Text,par1), new Parametr("ФИО", FIO.Text,par2), new Parametr("Оклад",OkM.Text,par3));
            KoO.Text = "";
            FIO.Text = "";
            OkM.Text  ="999999999";
            ShowDG();
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            SC.DeleteRows(DG, TB, "Вожатый");
            ShowDG();
        }

        private void KoO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                O_Window w = new O_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoO.Text = "" + SC.IDS("Select top 1 Код From Отряд Where Название like @Naz Order by 1",
                    new Parametr("@Naz", "%" + Naz.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                OA_Window w = new OA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (DG.SelectedItem != null)
                ((Owning)Owner).SetVal((DataRowView)DG.SelectedItem,7);
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }

        private void OkM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.D0 | e.Key == Key.D1 | e.Key == Key.D2 | e.Key == Key.D3 | e.Key == Key.D4 |
                e.Key == Key.D5 | e.Key == Key.D6 | e.Key == Key.D7 | e.Key == Key.D8 | e.Key == Key.D9 |
                e.Key == Key.OemPeriod | e.Key == Key.Back)
                return;
            else
                e.Handled = true;
        }
    }
}
