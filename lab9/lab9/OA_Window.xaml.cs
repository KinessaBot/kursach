﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for OA_Window.xaml
    /// </summary>
    public partial class OA_Window : Window
    {
        public OA_Window()
        {
            InitializeComponent();
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter("Insert into Отряд(Название) values(@naz)", SB, new SqlParameter("@naz", Naz.Text));
            if (Owner != null)
            {               
                ((Owning)Owner).SetVal(SC.IDS("Select Max(Код) From Отряд Where Название=@Naz", new Parametr("@Naz", Naz.Text)),1);
                Close();
            }
            Naz.Text = "";               
        }
    }
}
