﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for ZLA_Window.xaml
    /// </summary>
    public partial class ZLA_Window : Window, Owning
    {
        public void SetVal(DataRowView obj, int ctn = 1)
        {
            switch (ctn)
            {
                case 1:
                    {
                        KoO.Text = obj[0].ToString();
                        Naz.Text = obj[1].ToString();
                        break;
                    }
                case 3:
                    {
                        KoR.Text = obj[0].ToString();
                        FIO.Text = obj[1].ToString();
                        break;
                    }
                case 2:
                    {
                        KoZ.Text = obj[0].ToString();
                        DnM.Text = obj[1].ToString();
                        break;
                    }
                case 4:
                    {
                        KoK.Text = obj[0].ToString();
                        Nom.Text = obj[1].ToString();
                        break;
                    }
            }
        }

        public void SetVal(int kod, int ctn = 1)
        {
            switch (ctn)
            {
                case 1:
                    {
                        KoO.Text = "" + kod;
                        break;
                    }
                case 2:
                    {
                        KoZ.Text = "" + kod;
                        break;
                    }
                case 3:
                    {
                        KoR.Text = "" + kod;
                        break;
                    }
                case 4:
                    {
                        KoK.Text = "" + kod;
                        break;
                    }
            }
        }

        public ZLA_Window()
        {
            InitializeComponent();
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter("insert into ЗаездныйЛист(КодОтряда,КодЗаезда,КодРебенка,КодКомнаты,Цена,ДатаПокупки) values(@KoO,@KoZ,@KoR,@KoK,@CeM,@Dat)",
            SB, new Parametr("@KoO", KoO.Text),new Parametr("@KoZ", KoZ.Text), new Parametr("@KoR", KoR.Text),
            new Parametr("@KoK", KoK.Text), new Parametr("@CeM", CeM.Text), new Parametr("@Dat", Dat.DisplayDate));
            KoO.Text = "";
            Naz.Text = "";
            KoR.Text = "";
            FIO.Text = "";
            KoZ.Text = "";
            DnM.Text = "";
            KoK.Text = "";
            Nom.Text = "";
            Dat.Text = "";
            CeM.Text = "";                                
        }

        private void KoO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                O_Window w = new O_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoO.Text = "" + SC.IDS("Select top 1 Код From Отряд Where Название like @Naz Order by 1",
                     new Parametr("@Naz", "%" + Naz.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                OA_Window w = new OA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void Naz_KeyDown(object sender, KeyEventArgs e)
        {
            if (DnM.Text == "") DnM.Text = "3000-01-01";
            if (e.Key == Key.F1)
            {
                Z_Window w = new Z_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoZ.Text = "" + SC.IDS("Select top 1 Код From Заезд Where ДатаНачала<@DnM and ДатаНачала > @DnB Order by 1",
                    new Parametr("@DnM", DnM.DisplayDate));
            }
            if (e.Key == Key.F3)
            {
                ZA_Window w = new ZA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void KoR_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                R_Window w = new R_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoR.Text = "" + SC.IDS("Select top 1 Код From Ребенок Where ФИО like @FIO Order by 1",
                    new Parametr("@FIO", "%" + FIO.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                RA_Window w = new RA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void KoK_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                K_Window w = new K_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoK.Text = "" + SC.IDS("Select top 1 Код From Комната Where Номер like @Nom Order by 1",
                    new Parametr("@Nom", "%" + Nom.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                KA_Window w = new KA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void CeM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.D0 | e.Key == Key.D1 | e.Key == Key.D2 | e.Key == Key.D3 | e.Key == Key.D4 |
                e.Key == Key.D5 | e.Key == Key.D6 | e.Key == Key.D7 | e.Key == Key.D8 | e.Key == Key.D9 |
                e.Key == Key.OemPeriod | e.Key == Key.Back)
                return;
            else
                e.Handled = true;
        }
    }
}
