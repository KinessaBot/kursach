﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {
        DataTable VM;
        public static SqlConnection connection;
        public MainWindow()
        {
            try
            {
                InitializeComponent();
                GetSett();
                connection = new SqlConnection(SC.connection);
                connection.Open();
            }
            catch
            {
                MessageBox.Show("Неудается подключится к базе данных");
            }
        }

            
        public static void GetSett()
        {
            using (StreamReader sr = new StreamReader("sett.txt", Encoding.Default))
            {
                SC.connection = @""+sr.ReadLine();
                SC.path = @"" + sr.ReadLine();
            }
        }

        public static void Connect()
        {
            connection = new SqlConnection(SC.connection);
            connection.Open();
        }

        public void VMS_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            VM_WINDOW w = new VM_WINDOW();
            w.Show();
        }

        public void O_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            O_Window w = new O_Window();
            w.Show();
        }

        private void VMA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            VMA_Window w = new VMA_Window();
            w.Show();
        }

        private void Z_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Z_Window w = new Z_Window();
            w.Show();
        }

        private void K_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            K_Window w = new K_Window();
            w.Show();
        }

        private void R_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            R_Window w = new R_Window();
            w.Show();
        }

        private void V_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            V_Window w = new V_Window();
            w.Show();
        }

        private void OA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            OA_Window w = new OA_Window();
            w.Show();
        }

        private void VA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            VA_Window w = new VA_Window();
            w.Show();
        }

        private void KA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            KA_Window w = new KA_Window();
            w.Show();
        }

        private void RA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            RA_Window w = new RA_Window();
            w.Show();
        }

        private void ZA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            ZA_Window w = new ZA_Window();
            w.Show();
        }

        private void ZL_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            ZL_Window w = new ZL_Window();
            w.Show();
        }

        private void ZLA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            ZLA_Window w = new ZLA_Window();
            w.Show();
        }

        private void M_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            M_Window w = new M_Window();
            w.Show();
        }

        private void MA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MA_Window w = new MA_Window();
            w.Show();
        }

        private void OM_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            OM_Window w = new OM_Window();
            w.Show();
        }

        private void WM_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            WM_Window w = new WM_Window();
            w.Show();
        }

        private void WMA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            WMA_Window w = new WMA_Window();
            w.Show();
        }

        private void OMA_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            OMA_Window w = new OMA_Window();
            w.Show();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            REP1_Window w = new REP1_Window(1);
            w.Show();
        }

        private void Set_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Set_Window w = new Set_Window();
            w.Show();
        }

        private void MenuItem2_Click(object sender, RoutedEventArgs e)
        {
            REP1_Window w = new REP1_Window(2);
            w.Show();
        }

        private void MenuItem3_Click(object sender, RoutedEventArgs e)
        {
            REP1_Window w = new REP1_Window(3);
            w.Show();
        }

        private void SQL_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            SQL_Window w = new SQL_Window();
            w.Show();
        }

        private void func3_MenuItem3_Click(object sender, RoutedEventArgs e)
        {
            FUNC_Window w = new FUNC_Window(3);
            w.Show();
        }

        private void func2_MenuItem2_Click(object sender, RoutedEventArgs e)
        {
            FUNC_Window w = new FUNC_Window(2);
            w.Show();
        }

        private void func1_MenuItem_Click(object sender, RoutedEventArgs e)
        {
            FUNC_Window w = new FUNC_Window(1);
            w.Show();
        }
    }
}
