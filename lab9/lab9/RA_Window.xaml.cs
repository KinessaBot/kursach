﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for RA_Window.xaml
    /// </summary>
    public partial class RA_Window : Window
    {
        public RA_Window()
        {
            InitializeComponent();
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter("Insert into Ребенок(ФИО,ДатаРождения) values(@FIO,@DrM)", SB, new SqlParameter("@FIO", FIO.Text)
                , new SqlParameter("@DrM", DrM.Text));
            if (Owner != null)
            {
                
               ((Owning)Owner).SetVal(SC.IDS("Select Max(Код) From Ребенок Where ФИО=@FIO and ДатаРождения=@DrM", new Parametr("@FIO", FIO.Text),new Parametr("@DrM",DrM.Text)),3);
                Close();
            }
            FIO.Text = "";
            DrM.Text = "";
        }
    }
}
