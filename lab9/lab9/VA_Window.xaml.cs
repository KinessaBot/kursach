﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for VA_Window.xaml
    /// </summary>
    public partial class VA_Window : Window, Owning
    {
        public VA_Window()
        {
            InitializeComponent();
        }

        public void SetVal(DataRowView obj, int ctn = 1)
        {
            KoO.Text = obj[0].ToString();
        }

        public void SetVal(int kod,int ctn = 1)
        {
            KoO.Text = ""+kod;
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter("insert into Вожатый(ФИО,КодОтряда, Оклад) values(@FIO,@KoO,@OkM)", SB, new Parametr("@FIO", FIO.Text),
            new Parametr("@KoO", KoO.Text), new Parametr("@OkM", OkM.Text));
            if (Owner != null)
            {             
                ((Owning)Owner).SetVal(SC.IDS("Select Max(Код) From Вожатый Where ФИО=@FIO and КодОтряда=@KoO and Оклад=@OkM"
                   ,new Parametr("@FIO", FIO.Text),new Parametr("@KoO", KoO.Text), new Parametr("@OkM", OkM.Text)),7);
                Close();
            }
            FIO.Text = "";
            KoO.Text = "";
            OkM.Text = "";
        }


        private void KoO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                O_Window w = new O_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoO.Text = "" + SC.IDS("Select top 1 Код From Отряд Where Название like @Naz Order by 1",
                    new Parametr("@Naz", "%" + Naz.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                OA_Window w = new OA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void OkM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.D0 | e.Key == Key.D1 | e.Key == Key.D2 | e.Key == Key.D3 | e.Key == Key.D4 |
                e.Key == Key.D5 | e.Key == Key.D6 | e.Key == Key.D7 | e.Key == Key.D8 | e.Key == Key.D9 |
                e.Key == Key.OemPeriod | e.Key == Key.Back)
                return;
            else
                e.Handled = true;
        }
    }
}
