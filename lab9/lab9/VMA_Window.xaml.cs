﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for VMA_Window.xaml
    /// </summary>
    public partial class VMA_Window : Window
    {
        public VMA_Window()
        {
            InitializeComponent();
        }


        private void AB_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SqlCommand command = new SqlCommand("insert into ВидМероприятия(Описание) values(@Opis)", MainWindow.connection);
                command.Parameters.Add(new SqlParameter("@Opis", Opis.Text));
                if (Owner != null)
                {
                    ((Owning)Owner).SetVal(5, SC.IDS("Select Max(Код) From ВидМероприятия Where Описание=@Opis", new Parametr("@Opis", Opis.Text)));
                    Close();
                }
                Opis.Text = "";
                SB.Content = "Успешно добавлены(а) " + command.ExecuteNonQuery() + " запись(си)";
            }
            catch(Exception ex)
            {
                SB.Content = "Ошибка добавления" + ex.Message;
            }
        }

    }
}
