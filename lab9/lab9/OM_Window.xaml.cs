﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for OM_Window.xaml
    /// </summary>
    public partial class OM_Window : Window, Owning
    {
        DataTable O;
        SqlDataAdapter adap;

        public void SetVal(DataRowView obj, int ctn = 1)
        {
            if (ctn == 1)
            {
                KoO.Text = obj[0].ToString();
                Naz.Text = obj[1].ToString();
            }
            if (ctn == 6)
            {
                KoM.Text = obj[0].ToString();
                NazM.Text = obj[1].ToString();
            }
        }

        public void SetVal(int kod,int ctn = 1)
        {
            if (ctn == 1)
            {
                KoO.Text = ""+kod;
            }
            if (ctn == 6)
            {
               KoM.Text = "" + kod;
            }
        }

        public OM_Window()
        {
            InitializeComponent();
            DG.IsReadOnly = true;
        }

        private void ShowDG()
        {
            SC.QueryExecuter("exec OMS @Kod, @KoO, @KoM, @Naz, @NazM, @Res",
            DG, out O, ref adap, TB, new Parametr("@Kod", "%" + Kod.Text + "%"), new Parametr("@KoO", "%" + KoO.Text + "%"),
             new Parametr("@KoM", "%" + KoM.Text + "%"), new Parametr("@Naz", "%" + Naz.Text + "%"),
             new Parametr("@NazM", "%" + NazM.Text + "%"), new Parametr("@Res", "%" + Res.Text + "%"));
            KoO.Text = "";
            KoM.Text = "";
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            bool par1 = false, par2 = false, par3 = false;
            if (KoO.Text != "") par1 = true;
            if (KoM.Text != "") par2 = true;
            if (Res.Text != "") par3 = true;
            if (!par1 && !par2 && !par3)
                TB.Text = "Заполните хотя бы одно поле для изменения";
            SC.UpdateDB(DG, TB, "ОтрядНаМероприятии", new Parametr("КодОтряда", KoO.Text, par1), new Parametr("КодМероприятия", KoM.Text, par2),
                new Parametr("Результат", Res.Text, par3));
            KoO.Text = "";
            KoM.Text = "";
            Res.Text = "";
            ShowDG();
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            SC.DeleteRows(DG, TB, "ОтрядНаМероприятии");
            ShowDG();
        }

        private void KoO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                O_Window w = new O_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoO.Text = "" + SC.IDS("Select top 1 Код From Отряд Where Название like @Naz Order by 1",
                    new Parametr("@Naz", "%" + Naz.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                OA_Window w = new OA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void NazM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                M_Window w = new M_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                int x = SC.IDS("Select top 1 Код From Мероприятие Where Название like @NazM Order by 1", "Мероприятие",
                    new Parametr("Название", "%" + NazM.Text + "%"),
                    new Parametr("@NazM", "%" + NazM.Text + "%"));
                KoM.Text = "" + x;
            }
            if (e.Key == Key.F3)
            {
                MA_Window w = new MA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }
    }
}
