﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for OMA_Window.xaml
    /// </summary>
    public partial class OMA_Window : Window, Owning
    {
        public OMA_Window()
        {
            InitializeComponent();
        }
        public void SetVal(DataRowView obj, int ctn = 1)
        {
            if (ctn == 1)
            {
                KoO.Text = obj[0].ToString();
                Naz.Text = obj[1].ToString();
            }
            if (ctn == 6)
            {
                KoM.Text = obj[0].ToString();
                NazM.Text = obj[1].ToString();
            }
        }
        public void SetVal(int kod, int ctn = 1)
        {
            if (ctn == 1)
            {
                KoO.Text = "" + kod;
            }
            if (ctn == 6)
            {
                KoM.Text = "" + kod;
            }
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter("Insert into ОтрядНаМероприятии(КодМероприятия, КодОтряда, Результат) values(@KoM,@KoO,@Res)",
                SB, new SqlParameter("@KoM", KoM.Text), new SqlParameter("@KoO", KoO.Text), new SqlParameter("@Res", Res.Text));
            KoO.Text = "";
            Naz.Text = "";
            KoM.Text = "";
            NazM.Text = "";
            Res.Text = "";
        }

        private void KoO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                O_Window w = new O_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoO.Text = "" + SC.IDS("Select top 1 Код From Отряд Where Название like @Naz Order by 1",
                     new Parametr("@Naz", "%" + Naz.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                OA_Window w = new OA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void NazM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                M_Window w = new M_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                int x = SC.IDS("Select top 1 Код From Мероприятие Where Название like @NazM Order by 1", "Мероприятие",
                    new Parametr("Название", "%" + NazM.Text + "%"),
                    new Parametr("@NazM", "%" + NazM.Text + "%"));
                KoM.Text = "" + x;
            }
            if (e.Key == Key.F3)
            {
                MA_Window w = new MA_Window();
                w.Owner = this;
                w.Show();
            }
        }
    }
}
