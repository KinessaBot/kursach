﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace lab9
{
    /// <summary>
    /// Interaction logic for Set_Window.xaml
    /// </summary>
    public partial class Set_Window : Window
    {
        public Set_Window()
        {
            InitializeComponent();
            using (StreamReader sr = new StreamReader("sett.txt", Encoding.Default))
            {
                connection.Text =  @""+sr.ReadLine();
                path.Text =  @""+sr.ReadLine();
            }
            XtraReport1 xtraReport = new XtraReport1();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            using (StreamWriter sw = new StreamWriter("sett.txt", false, Encoding.Default))
            {
                sw.WriteLine(connection.Text);
                sw.WriteLine(path.Text);
            }
            MainWindow.GetSett();
            MainWindow.Connect();
        }
    }
}
