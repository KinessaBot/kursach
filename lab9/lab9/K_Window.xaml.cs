﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for K_Window.xaml
    /// </summary>
    public partial class K_Window : Window
    {

        DataTable K;
        SqlDataAdapter adap;

        public K_Window()
        {
            InitializeComponent();
            
        }

       

        private void ShowDG()
        {
            SC.QueryExecuter("exec KS @Kod, @Nom, @Sos, @NVS", DG, out K, ref adap, TB,new Parametr("@Kod", "%" + Kod.Text + "%"),
               new Parametr("@Nom", "%" + Nom.Text + "%"), new Parametr("@Sos",Sos.IsChecked), new Parametr("@NVS", NVS.IsChecked));
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            SC.UpdateDB(adap, K, TB);
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            SC.DeleteRows(DG, adap, K,TB);
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (DG.SelectedItem != null)
                ((Owning)Owner).SetVal((DataRowView)DG.SelectedItem,4);
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }

        private void Nom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.D0 | e.Key == Key.D1 | e.Key == Key.D2 | e.Key == Key.D3 | e.Key == Key.D4 |
                e.Key == Key.D5 | e.Key == Key.D6 | e.Key == Key.D7 | e.Key == Key.D8 | e.Key == Key.D9 | 
                e.Key == Key.OemMinus | e.Key== Key.Back)
                return;
            else
                e.Handled = true;
        }
    }
}
