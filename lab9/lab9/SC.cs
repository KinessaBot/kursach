﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Controls;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Windows;

namespace lab9
{
    static class SC
    {
        static public string path { get; set; } = "C:/Users/Seni/Desktop/";
        static public string connection { get; set; } = @"Data Source=SENI-PC\SQLEXPRESS;Initial Catalog=NewCSC;Integrated Security=True";
        static public DataContractJsonSerializer jsonFor = new DataContractJsonSerializer(typeof(LogJSON));
        static public string maxDateString ="01.01.3000";
        static public string minDateString ="01.01.1900";

        static public void UpdateDB(SqlDataAdapter adap, DataTable dt, TextBlock TB)
        {
            try
            {
                SqlCommandBuilder cm = new SqlCommandBuilder(adap);
                adap.Update(dt);
                TB.Text = "Выполенено успешно";
            }
            catch (Exception ex)
            {
                TB.Text = ex.Message;
            }
        }

        static public bool UpdateDB(DataGrid DG, TextBlock TB, string table, params Parametr[] parametrs)
        {
            try
            {
                if (DG.SelectedItems != null)
                {
                    for (int i = 0; i < DG.SelectedItems.Count; i++)
                    {
                        int kod = (int)((DataRowView)DG.SelectedItems[i])[0];
                        string query = "Update " + table + " Set";
                        for (int j = 0; j < parametrs.Length; j++)
                            if (parametrs[j].needed)
                                query += " " + parametrs[j].InitString + " = '" + parametrs[j].RelizObject + "',";
                        query = query.Substring(0, query.Length - 1) + " Where Код=" + kod;
                        SqlCommand command = new SqlCommand(query, MainWindow.connection);
                        TB.Text = "Успешно обновлены(а) " + command.ExecuteNonQuery() + " запись(си)";
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                TB.Text = "Ошибка обновления " + ex.Message;
            }
            return false;
        }

        static public void DeleteRows(DataGrid DG, SqlDataAdapter adap, DataTable dt, TextBlock TB)
        {
            try
            {
                if (DG.SelectedItems != null)
                {
                    for (int i = 0; i < DG.SelectedItems.Count; i++)
                    {
                        DataRowView datarowView = DG.SelectedItems[i] as DataRowView;
                        if (datarowView != null)
                        {
                            DataRow dataRow = (DataRow)datarowView.Row;
                            dataRow.Delete();
                        }
                    }
                }
                SC.UpdateDB(adap, dt, TB);
            }
            catch
            { }
        }

        static public bool DeleteRows(DataGrid DG, TextBlock TB, string table)
        {
            try
            {
                if (DG.SelectedItems != null)
                {
                    for (int i = 0; i < DG.SelectedItems.Count; i++)
                    {
                        int kod = (int)((DataRowView)DG.SelectedItems[i])[0];
                        SqlCommand command = new SqlCommand("Delete From " + table + " Where Код=" + kod, MainWindow.connection);
                        TB.Text = "Успешно удалены(а) " + command.ExecuteNonQuery() + " запись(си)";
                    }
                    return true;
                }

            }
            catch (Exception ex)
            {
                TB.Text = "Ошибка удаления" + ex.Message;               
            }
            return false;
        }

        static public void QueryExecuter(string Query, DataGrid DG, out DataTable VM, ref SqlDataAdapter adap, TextBlock TB, params Parametr[] parametrs)
        {
            try
            {
                VM = new DataTable();
                SqlCommand command = new SqlCommand(Query, MainWindow.connection);
                for (int i = 0; i < parametrs.Length; i++)
                    command.Parameters.Add(new SqlParameter(parametrs[i].InitString, parametrs[i].RelizObject));
                adap = new SqlDataAdapter(command);
                adap.Fill(VM);
                DG.ItemsSource = VM.DefaultView;
                TB.Text = "Выполенено успешно";
                DG.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                TB.Text = ex.Message;
                VM = new DataTable();
            }
        }

        static public void QueryExecuter(string Query, DataGrid DG, out DataTable VM, ref SqlDataAdapter adap, TextBlock TB)
        {
            try
            {
                VM = new DataTable();
                SqlCommand command = new SqlCommand(Query, MainWindow.connection);
                adap = new SqlDataAdapter(command);
                adap.Fill(VM);
                DG.ItemsSource = VM.DefaultView;
                TB.Text = "Запрос выполнен успешно";
                DG.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                TB.Text = ex.Message;
                VM = new DataTable();
            }
        }

        static public void QueryExecuter(string Query, DataGrid DG, out DataTable VM, ref SqlDataAdapter adap, TextBlock TB, params SqlParameter[] parametrs)
        {
            try
            {
                VM = new DataTable();
                SqlCommand command = new SqlCommand(Query, MainWindow.connection);
                for (int i = 0; i < parametrs.Length; i++)
                    command.Parameters.Add(parametrs[i]);
                adap = new SqlDataAdapter(command);
                adap.Fill(VM);
                DG.ItemsSource = VM.DefaultView;
                TB.Text = "Выполенено успешно";
                DG.Columns[0].Visibility = Visibility.Hidden;
            }
            catch (Exception ex)
            {
                TB.Text = ex.Message;
                VM = new DataTable();
            }
        }

        static public bool QueryExecuter(string query, Label SB, params Parametr[] parametrs)
        {
            try
            {
                SqlCommand command = new SqlCommand(query, MainWindow.connection);
                for (int i = 0; i < parametrs.Length; i++)
                    command.Parameters.Add(new SqlParameter(parametrs[i].InitString, parametrs[i].RelizObject));
                SB.Content = "Успешно добавлены(а) " + command.ExecuteNonQuery() + " запись(си)";
                return true;
            }
            catch (Exception ex)
            {
                SB.Content = "Ошибка добавления" + ex.Message;
                return false;
            }
        }

        static public void QueryExecuter(string query, Label SB, params SqlParameter[] parametrs)
        {
            try
            {
                SqlCommand command = new SqlCommand(query, MainWindow.connection);
                for (int i = 0; i < parametrs.Length; i++)
                    command.Parameters.Add(parametrs[i]);
                SB.Content = "Успешно добавлены(а) " + command.ExecuteNonQuery() + " запись(си)";
            }
            catch (Exception ex)
            {
                SB.Content = "Ошибка добавления" + ex.Message;
            }
        }

        static public int IDS(string query, params Parametr[] param)
        {
            try
            {
                SqlCommand command = new SqlCommand(query, MainWindow.connection);
                for (int i = 0; i < param.Length; i++)
                    command.Parameters.Add(new SqlParameter(param[i].InitString, param[i].RelizObject));
                return (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        static public int IDS(string query, string table, Parametr sp, params Parametr[] param)
        {
            try
            {
                SqlCommand command = new SqlCommand(query, MainWindow.connection);
                for (int i = 0; i < param.Length; i++)
                    command.Parameters.Add(new SqlParameter(param[i].InitString, param[i].RelizObject));
                if (1 != SC.GetRowNumber(table, sp))
                {
                    MessageBox.Show("Выбор не уникальный. Используйте выбор или уточните");
                    throw new Exception();
                }
                return (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        static public int GetRowNumber(string table, Parametr sp)
        {
            try
            {
                SqlCommand command = new SqlCommand(@"Select count(*) From " + table + " Where "+sp.InitString+" like '" + sp.RelizObject+"'", MainWindow.connection);
                return (int)command.ExecuteScalar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка подключения" + ex.Message);
                return -1;
            }
        }
    }



    public class Parametr
    {
        public string InitString { get; set; }
        public object RelizObject { get; set; }
        public bool needed { get; set; }

        public Parametr(string InitString, object RelizObject, bool needed = false)
        {
            this.InitString = InitString;
            this.RelizObject = RelizObject;
            this.needed = needed;
        }
    }

    [Serializable]
    public class LogJSON
    {
        public enum Action
        {
            Insert =1,
            Update,
            Delete
        }
        public string table { get; set; }
        public Column[] columns { get; set; }
        public Action action { get; set; }
        public DateTime dateTime { get; set; }

        public LogJSON(string table, Action action, DateTime dateTime, params Column[] columns)
        {
            this.table = table;
            this.columns = columns;
            this.action = action;
            this.dateTime = dateTime;
        }
    }

    [Serializable]
    public class Column
    {
        public string name { get; set; }
        public object oldValue { get; set; }
        public object newValue { get; set; }

        public Column(string name, object oldValue, object newValue, bool needed)
        {
            this.name = name;
            this.oldValue = oldValue;
            if(needed)
                this.newValue = newValue;
            else
                this.newValue = null;
        }
    }
}
