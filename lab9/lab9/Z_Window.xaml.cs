﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace lab9
{
    /// <summary>
    /// Interaction logic for Z_Window.xaml
    /// </summary>
    public partial class Z_Window : Window
    {

        DataTable Z;
        SqlDataAdapter adap;

        public Z_Window()
        {
            InitializeComponent();
        }

        private void ShowDG()
        {
            if (DNM.Text == "") DNM.Text = SC.maxDateString;
            if (DNB.Text == "") DNB.Text = SC.minDateString;
            if (DOM.Text == "") DOM.Text = SC.maxDateString;
            if (DOB.Text == "") DOB.Text = SC.minDateString;
            if (DM.Text == "") DM.Text = "999999999";
            if (DB1.Text == "") DB1.Text = "-999999999";
            SC.QueryExecuter("exec ZS @Kod, @DL, @DNM, @DNB, @DOM, @DOB, @DM, @DB", DG, out Z, ref adap
                , TB, new SqlParameter("@Kod", "%" + Kod.Text + "%"), new SqlParameter("@DL", "%" + DL.Text + "%"),
                new SqlParameter("@DNM", DNM.DisplayDate), new SqlParameter("@DNB", DNB.DisplayDate), new SqlParameter("@DOM", DOM.DisplayDate),
                new SqlParameter("@DOB", DOB.DisplayDate), new SqlParameter("@DM", DM.Text), new SqlParameter("@DB", DB1.Text));
            
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            SC.UpdateDB(adap, Z, TB);
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            SC.DeleteRows(DG, adap, Z, TB);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (DG.SelectedItem != null)
                ((Owning)Owner).SetVal((DataRowView)DG.SelectedItem,2);
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }
    }
}

