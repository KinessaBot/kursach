﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lab9
{
    /// <summary>
    /// Interaction logic for REP1_Window.xaml
    /// </summary>
    public partial class REP1_Window : Window
    {
        int report;
        public REP1_Window(int report)
        {
            InitializeComponent();
            this.report = report;
        }

        private void BS_Click(object sender, RoutedEventArgs e)
        {
            switch (report)
            {
                case 1:
                    {
                        XtraReport1 rep1 = new XtraReport1();
                        rep1.Parameters[0].Value = date.DisplayDate;
                        rep1.CreateDocument(true);
                        rep1.ExportToPdf(SC.path + "Комнаты_Детей_Отчет_" + DateTime.Today.ToString("d") + ".pdf");
                        Close();
                        break;
                    }
                case 2:
                    {
                        XtraReport2 rep1 = new XtraReport2();
                        rep1.Parameters[0].Value = date.DisplayDate;
                        rep1.CreateDocument(true);
                        rep1.ExportToPdf(SC.path + "Доход_в_заездах" + DateTime.Today.ToString("d") + ".pdf");
                        Close();
                        break;
                    }
                case 3:
                    {
                        XtraReport3 rep1 = new XtraReport3();
                        rep1.CreateDocument(true);
                        rep1.ExportToPdf(SC.path + "Отряд_на_мероприятии" + DateTime.Today.ToString("d") + ".pdf");
                        Close();
                        break;
                    }
                
            }
            Close();
        }
    }
}
