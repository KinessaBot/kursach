﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for ZL_Window.xaml
    /// </summary>
    public partial class ZL_Window : Window, Owning
    {
        DataTable O;
        SqlDataAdapter adap;

        public void SetVal(DataRowView obj, int ctn=1)
        {
            switch(ctn)
            {
                case 1:
                    {
                        KoO.Text = obj[0].ToString();
                        Naz.Text = obj[1].ToString();
                        break;
                    }
                case 3:
                    {
                        KoR.Text = obj[0].ToString();
                        FIO.Text = obj[1].ToString();
                        break;
                    }
                case 2:
                    {
                        KoZ.Text = obj[0].ToString();
                        DnM.Text = obj[1].ToString();
                        break;
                    }
                case 4:
                    {
                        KoK.Text = obj[0].ToString();
                        Nom.Text = obj[1].ToString();
                        break;
                    }
            }   
        }

        public void SetVal(int kod,int ctn = 1)
        {
            switch (ctn)
            {
                case 1:
                    {
                        KoO.Text = ""+kod;
                        break;
                    }
                case 2:
                    {
                        KoZ.Text = "" + kod;
                        break;
                    }
                case 3:
                    {
                        KoR.Text = "" + kod;
                        break;
                    }
                case 4:
                    {
                        KoK.Text = "" + kod;
                        break;
                    }
            }
        }

        public ZL_Window()
        {
            InitializeComponent();
            DG.IsReadOnly = true;
        }

        private void ShowDG()
        {
            if (DnM.Text == "") DnM.Text = SC.maxDateString;
            if (DnB.Text == "") DnB.Text = SC.minDateString;
            if (CeM.Text == "") CeM.Text = "9999999999";
            if (CeB.Text == "") CeB.Text = "-9999999999";
            SC.QueryExecuter("exec ZLS @FIO, @Kod, @KoZ, @KoO, @KoK,@KoR, @Nom, @Naz, @DnM, @DnB,@CeM,@CeB",
            DG, out O, ref adap, TB, new Parametr("@Kod", "%" + Kod.Text + "%"),new Parametr("@Naz", "%" + Naz.Text + "%"),
            new Parametr("@FIO", "%" + FIO.Text + "%"), new Parametr("@KoO", "%" + KoO.Text + "%"),
            new Parametr("@CeM", CeM.Text), new Parametr("@CeB", CeB.Text), new Parametr("@KoZ", "%" + KoZ.Text+"%" )
            , new Parametr( "@KoR", "%" + KoR.Text + "%"), new Parametr("@KoK", "%" + KoK.Text + "%")
            , new Parametr("@Nom", "%" + Nom.Text + "%"), new Parametr("@DnM", DnM.DisplayDate),
            new Parametr("@DnB", DnB.DisplayDate));
            KoO.Text = "";
            KoR.Text = "";
            KoZ.Text = "";
            KoK.Text = "";
            /*if (DnM.Text == "") DnM.Text = "3000-01-01";
            if (DnB.Text == "") DnB.Text = null;
            if (CeM.Text == "") CeM.Text = "9999999999";
            if (CeB.Text == "") CeB.Text = "-9999999999";
            SC.QueryExecuter("exec ZLS @FIO, '%" + Kod.Text + "%', '%" + KoZ.Text + "%', '%" + KoO.Text + "%', '%" + KoK.Text + "%','%" + KoR.Text + "%', '%" + Nom.Text + "%', '%" + Naz.Text + "%', '"+DnM.Text+ "', '" + DnB.Text + "' , " + CeM.Text + " , " + CeB.Text,
            DG, out O, ref adap, new SqlParameter("@FIO",FIO.Text));*/
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            bool par1 = false, par2 = false, par3 = false, par4 = false
                , par5 = false;
            if (KoR.Text != "") par1 = true;
            if (CeM.Text != "9999999999") par2 = true;
            if (KoZ.Text != "") par3 = true;
            if (KoK.Text != "") par4 = true;
            if (KoO.Text != "") par5 = true;
            if (!par1 && !par2 && !par3 && !par4 && !par5)
                TB.Text = "Заполните хотя бы одно поле для изменения";
            SC.UpdateDB(DG, TB, "ЗаездныйЛист", new Parametr("КодОтряда", KoO.Text, par5), new Parametr("КодЗаезда", KoZ.Text, par3),
                new Parametr("КодРебенка", KoR.Text, par1), new Parametr("КодКомнаты", KoK.Text, par4), new Parametr("Цена", CeM.Text, par2));
            KoO.Text = "";
            KoR.Text = "";
            CeM.Text = "9999999999";
            KoZ.Text = "";
            KoK.Text = "";
            ShowDG();
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            SC.DeleteRows(DG, TB, "ЗаездныйЛист");
            ShowDG();
        }

        private void KoO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                O_Window w = new O_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoO.Text = "" + SC.IDS("Select top 1 Код From Отряд Where Название like @Naz Order by 1",
                    new Parametr("@Naz", "%" + Naz.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                OA_Window w = new OA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void Naz_KeyDown(object sender, KeyEventArgs e)
        {
            if (DnM.Text == "") DnM.Text = SC.maxDateString;
            if (DnB.Text == "") DnB.Text = SC.minDateString;
            if (e.Key == Key.F1)
            {
                Z_Window w = new Z_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoZ.Text = "" + SC.IDS("Select top 1 Код From Заезд Where ДатаНачала<@DnM and ДатаНачала > @DnB Order by 1",
                    new Parametr("@DnM", DnM.DisplayDate),new Parametr("@DnB", DnB.DisplayDate));
            }
            if (e.Key == Key.F3)
            {
                ZA_Window w = new ZA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void KoR_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                R_Window w = new R_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoR.Text = "" + SC.IDS("Select top 1 Код From Ребенок Where ФИО like @FIO Order by 1",
                    new Parametr("@FIO", "%" + FIO.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                RA_Window w = new RA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void KoK_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                K_Window w = new K_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoK.Text = "" + SC.IDS("Select top 1 Код From Комната Where Номер like @Nom Order by 1",
                    new Parametr("@Nom", "%" + Nom.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                KA_Window w = new KA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }

        private void CeM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.D0 | e.Key == Key.D1 | e.Key == Key.D2 | e.Key == Key.D3 | e.Key == Key.D4 |
                e.Key == Key.D5 | e.Key == Key.D6 | e.Key == Key.D7 | e.Key == Key.D8 | e.Key == Key.D9 |
                e.Key == Key.OemPeriod | e.Key == Key.Back)
                return;
            else
                e.Handled = true;
        }

    }


}

