﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for R_Window.xaml
    /// </summary>
    public partial class R_Window : Window
    {
        DataTable R;
        SqlDataAdapter adap;

        public R_Window()
        {
            InitializeComponent();
        }
       

        private void ShowDG()
        {
            if (DRM.Text == "") DRM.Text = SC.maxDateString;
            if (DRB.Text == "") DRB.Text = SC.minDateString;
            SC.QueryExecuter("exec RS @Kod, @Fio, @DRM, @DRB", DG, out R, ref adap, TB, new Parametr("@Kod", "%" + Kod.Text + "%"),
               new Parametr("@Fio", "%" + FIO.Text + "%"),  new Parametr("@DRM", DRM.Text) , new Parametr("@DRB", DRB.Text ));
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            SC.UpdateDB(adap, R, TB);
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            SC.DeleteRows(DG, adap, R, TB);
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            if (DG.SelectedItem != null)
                ((Owning)Owner).SetVal((DataRowView)DG.SelectedItem,2);
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }
    }
}
