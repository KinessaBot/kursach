﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;


namespace lab9
{
    /// <summary>
    /// Interaction logic for WMA_Window.xaml
    /// </summary>
    public partial class WMA_Window : Window, Owning
    {
        public WMA_Window()
        {
            InitializeComponent();
        }

        public void SetVal(DataRowView obj, int ctn = 1)
        {
            if (ctn == 7)
            {
                KoV.Text = obj[0].ToString();
                FIO.Text = obj[1].ToString();
            }
            if (ctn == 6)
            {
                KoM.Text = obj[0].ToString();
                NazM.Text = obj[1].ToString();
            }
        }

        public void SetVal(int kod, int ctn = 1)
        {
            if (ctn == 7)
            {
                KoV.Text = "" + kod;
            }
            if (ctn == 6)
            {
                KoM.Text = "" + kod;
            }
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter("Insert into ВожатыйНаМероприятии(КодВожатого,КодМероприятия,Обязанность) values(@KoV,@KoM,@Dol)",
                SB, new SqlParameter("@KoV", KoV.Text), new SqlParameter("@KoM", KoM.Text), new SqlParameter("@Dol", Dol.Text));
            KoV.Text = "";
            Dol.Text = "";
            KoM.Text = "";
            NazM.Text = "";
            Dol.Text = "";
        }

        private void KoO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                V_Window w = new V_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoV.Text = "" + SC.IDS("Select top 1 Код From Вожатый Where ФИО like @FIO Order by 1",
                    new Parametr("@FIO", "%" + FIO.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                VA_Window w = new VA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void NazM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                ;
                M_Window w = new M_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                int x = SC.IDS("Select top 1 Код From Мероприятие Where Название like @NazM Order by 1", "Мероприятие",
                    new Parametr("Название", "%" + NazM.Text + "%"),
                    new Parametr("@NazM", "%" + NazM.Text + "%"));
                KoM.Text = "" + x;
            }
            if (e.Key == Key.F3)
            {
                MA_Window w = new MA_Window();
                w.Owner = this;
                w.Show();
            }
        }
    }
}

