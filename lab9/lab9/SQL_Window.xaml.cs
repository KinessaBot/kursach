﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for SQL_Window.xaml
    /// </summary>
    public partial class SQL_Window : Window
    {
        DataTable O;
        SqlDataAdapter adap;
        public SQL_Window()
        {
            InitializeComponent();
            DG.IsReadOnly = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter(TB.Text, DG, out O, ref adap, SB);
        }
    }
}
