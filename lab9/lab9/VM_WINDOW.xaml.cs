﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;


namespace lab9
{
    /// <summary>
    /// Interaction logic for VM_WINDOW.xaml
    /// </summary>
    public partial class VM_WINDOW : Window
    {

        DataTable VM;
        SqlDataAdapter adap;

        public VM_WINDOW()
        {     
            InitializeComponent();
        }

        private void ShowDG()
        {
            SC.QueryExecuter("exec VMS @Kod, @Opis", DG, out VM, ref adap, TB, new Parametr("@Kod", "%" + Kod.Text + "%"),
               new Parametr("@Opis", "%" + Opis.Text + "%"));           
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            SC.UpdateDB(adap, VM, TB);
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            SC.DeleteRows(DG, adap, VM, TB);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (DG.SelectedValue != null)
                ((Owning)Owner).SetVal((DataRowView)DG.SelectedValue,5);
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }
    }
}
