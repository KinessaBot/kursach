﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for ZA_Window.xaml
    /// </summary>
    public partial class ZA_Window : Window
    {
        public ZA_Window()
        {
            InitializeComponent();
        }

        private void AB_Click(object sender, RoutedEventArgs e)
        {
            SC.QueryExecuter("Insert into Заезд(ДатаНачала,ДатаОкончания) values(@DnM,@DoM)", SB, new SqlParameter("@DnM", DnM.DisplayDate)
                , new SqlParameter("@DoM", DoM.DisplayDate));
            if (Owner != null)
            {
                ((Owning)Owner).SetVal(SC.IDS("Select Max(Код) From Заезд Where ДатаНачала=@DnM and ДатаОкончания=@DoM", new Parametr("@DnM", DnM.DisplayDate)
                    , new Parametr("@DoM", DoM.DisplayDate)),2);
                Close();
            }
            DnM.Text = "";
            DoM.Text = "";
        }
    }
}
