﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace lab9
{
    /// <summary>
    /// Interaction logic for M_Window.xaml
    /// </summary>
    public partial class M_Window : Window, Owning
    {
        DataTable O;
        SqlDataAdapter adap;
        

        public void SetVal(DataRowView obj, int ctn = 1)
        {
            KoV.Text = obj[0].ToString();
            Ops.Text = obj[1].ToString();
        }

        public void SetVal(int kod, int ctn = 1)
        {
            KoV.Text = "" + kod;
        }

        public M_Window()
        {
            InitializeComponent();
            DG.IsReadOnly = true;
        }

        private void ShowDG()
        {
            if (DpM.Text == "") DpM.Text = SC.maxDateString;
            if (DpB.Text == "") DpB.Text = SC.minDateString;
            SC.QueryExecuter("exec MS @Kod, @KoV, @Naz, @Ops, @DpM, @DpB",
            DG, out O, ref adap, TB, new Parametr("@Kod", "%" + Kod.Text + "%"), new Parametr("@Naz", "%" + Naz.Text + "%"),
             new Parametr("@KoV", "%" + KoV.Text + "%"),new Parametr("@Ops", "%" + Ops.Text + "%"),
             new Parametr("@DpM", DpM.DisplayDate),new Parametr("@DpB", DpB.DisplayDate));
            KoV.Text = "";
            DG.Columns[4].Visibility = Visibility.Hidden;
        }

        private void UB_Click(object sender, RoutedEventArgs e)
        {
            bool par1 = false, par2 = false, par3 = false;
            if (KoV.Text != "") par1 = true;
            if (DpM.Text != SC.maxDateString) par2 = true;
            if (Naz.Text != "") par3 = true;
            if (!par1 && !par2 && !par3 )
                TB.Text = "Заполните хотя бы одно поле для изменения";
            if (SC.UpdateDB(DG, TB, "Мероприятие", new Parametr("КодВида", KoV.Text, par1), new Parametr("ДатаПроведения", DpM.DisplayDate, par2),
                new Parametr("Название", Naz.Text, par3)))
            {
                using (FileStream fs = new FileStream("Log.json", FileMode.Append))
                {
                    for (int i = 0; i < DG.SelectedItems.Count; i++)
                    {
                        SC.jsonFor.WriteObject(fs, new LogJSON("Мероприятие", LogJSON.Action.Update,
                            DateTime.Now, new Column("Код", ((DataRowView)DG.SelectedItems[i])[0], ((DataRowView)DG.SelectedItems[i])[0], true),
                            new Column("КодВида", ((DataRowView)DG.SelectedItems[i])[4], par1?int.Parse(KoV.Text):default(int),par1),
                            new Column("ДатаПроведения", ((DataRowView)DG.SelectedItems[i])[2], par2?DpM.DisplayDate:default(DateTime),par2),
                            new Column("Название", ((DataRowView)DG.SelectedItems[i])[1], par3?Naz.Text:default(string),par3)));
                    }
                }
            }
            KoV.Text = "";
            DpM.Text = SC.maxDateString;
            Naz.Text = "";
            ShowDG();
        }

        private void SB_Click(object sender, RoutedEventArgs e)
        {
            ShowDG();
        }

        private void DB_Click(object sender, RoutedEventArgs e)
        {
            if (SC.DeleteRows(DG, TB, "Мероприятие"))
            {
                using (FileStream fs = new FileStream("Log.json", FileMode.Append))
                {
                    for (int i = 0; i < DG.SelectedItems.Count; i++)
                    {
                        SC.jsonFor.WriteObject(fs, new LogJSON("Мероприятие", LogJSON.Action.Update,
                            DateTime.Now, new Column("Код", (int)((DataRowView)DG.SelectedItems[i])[0], null, false),
                            new Column("КодВида", (int)((DataRowView)DG.SelectedItems[i])[4], null, false),
                            new Column("ДатаПроведения", (DateTime)((DataRowView)DG.SelectedItems[i])[2], null, false),
                            new Column("Название", (string)((DataRowView)DG.SelectedItems[i])[1], null, false)));
                    }
                }
            }
            ShowDG();
        }

        private void KoV_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                VM_WINDOW w = new VM_WINDOW();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                KoV.Text = ""+SC.IDS("Select top 1 Код From ВидМероприятия Where Описание like @Ops Order by 1",
                     new Parametr("@ops", "%" + Ops.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                VMA_Window w = new VMA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (DG.SelectedItem != null)
                ((Owning)Owner).SetVal((DataRowView)DG.SelectedItem,6);
        }

        private void DG_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DG.SelectedIndex == -1 || !(DG.SelectedItems[0] is DataRowView))
                GR.Visibility = Visibility.Hidden;
            else
                GR.Visibility = Visibility.Visible;
        }
    }
}
