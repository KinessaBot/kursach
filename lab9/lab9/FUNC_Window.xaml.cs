﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;

namespace lab9
{
    /// <summary>
    /// Interaction logic for FUNC_Window.xaml
    /// </summary>
    public partial class FUNC_Window : Window, Owning
    {
        int func = 0;
        DataTable VM;
        SqlDataAdapter adap;

        public FUNC_Window(int func)
        {
            InitializeComponent();
            this.func = func;

            if (func != 2)
            {
                GR.Visibility = Visibility.Hidden;
            }
            else
            {
                GR.Visibility = Visibility.Visible;
            }
            if (func != 1)
            {
                Naz.Visibility = Visibility.Hidden;
                Naz1.Visibility = Visibility.Hidden;
            }
            else
            {
                Naz.Visibility = Visibility.Visible;
                Naz1.Visibility = Visibility.Visible;
            }

        }

        public void SetVal(DataRowView obj, int ctn = 1)
        {
            if(ctn!=1)            
                DnM.Text = obj[1].ToString();
            else
                Naz.Text = obj[1].ToString();
            Kod.Text = obj[0].ToString();
        }

        public void SetVal(int kod, int ctn = 1)
        {
            Kod.Text = "" + kod;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            switch (func)
            {
                case 1:
                    {
                        SC.QueryExecuter("Select * from ВожатыеВОтряде(@Kod)", DG, out VM,ref adap, TB, new Parametr("@Kod", Kod.Text));
                        DG.Columns[1].Visibility = Visibility.Hidden;
                        break;
                    }
                case 2:
                    {

                        SC.QueryExecuter("Select * from ПродажиЗаезда(@Kod)", DG, out VM, ref adap, TB, new Parametr("@Kod", Kod.Text));
                        DG.Columns[2].Visibility = Visibility.Hidden;
                        DG.Columns[3].Visibility = Visibility.Hidden;
                        DG.Columns[4].Visibility = Visibility.Hidden;
                        DG.Columns[5].Visibility = Visibility.Hidden;
                        break;
                    }
                case 3:
                    {
                        SC.QueryExecuter("Select *, dbo.ДоходСЗаезда(КодЗаезда) as Доход from  Заезд, ЗаездныйЛист where Заезд.Код = КодЗаезда", DG, out VM, ref adap, TB);
                        DG.Columns[3].Visibility = Visibility.Hidden;
                        DG.Columns[4].Visibility = Visibility.Hidden;
                        DG.Columns[5].Visibility = Visibility.Hidden;
                        DG.Columns[6].Visibility = Visibility.Hidden;
                        DG.Columns[7].Visibility = Visibility.Hidden;
                        DG.Columns[8].Visibility = Visibility.Hidden;
                        DG.Columns[11].Header = "Доход";
                        break;
                    }
            }
        }

        private void Kod_KeyDown(object sender, KeyEventArgs e)
        {
            if (DnM.Text == "") DnM.Text = SC.maxDateString;
            if (DnB.Text == "") DnB.Text = SC.minDateString;
            if (e.Key == Key.F1)
            {
                Z_Window w = new Z_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                Kod.Text = "" + SC.IDS("Select top 1 Код From Заезд Where ДатаНачала<@DnM and ДатаНачала > @DnB Order by 1",
                     new Parametr("@DnM", DnM.DisplayDate), new Parametr("@DnB", DnB.DisplayDate));
            }
            if (e.Key == Key.F3)
            {
                ZA_Window w = new ZA_Window();
                w.Owner = this;
                w.Show();
            }
        }

        private void Naz_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F1)
            {
                O_Window w = new O_Window();
                w.Owner = this;
                w.Show();
            }
            if (e.Key == Key.F2)
            {
                Kod.Text = "" + SC.IDS("Select top 1 Код From Отряд Where Название like @Naz Order by 1",
                    new Parametr("@Naz", "%" + Naz.Text + "%"));
            }
            if (e.Key == Key.F3)
            {
                OA_Window w = new OA_Window();
                w.Owner = this;
                w.Show();
            }
        }
    }
}
